var app = angular.module('notifications', []);


/**
 * Notifications controller
 */
app.controller('NotificationsListCtrl', function ($scope, notificationsService) {
    // Get notifications

    //notificationsService.getNotifications().then(function (data) {
    //    // Mock notifications
    //    data = [];
    //    for (var $i = 1; $i <= 10; $i++) {
    //        data.push({
    //            "id": $i,
    //            "user_id": $i,
    //            "device_id": $i,
    //            "message": "Hello there! You're a great Omneo user.",
    //            "json_payload_sent": {
    //                message: "Hello there! You're a great Omneo user."
    //            },
    //            "json_api_reply": "122456455stt778",
    //            "created_at": moment.unix(1443413387).fromNow(),
    //            "updated_at": moment.unix(1443413387).fromNow(),
    //            "deleted_at": null
    //        });
    //    }
    //
    //    $scope.notifications = data;
    //});
});

/**
 * Notifications Test Controller
 */
app.controller('NotificationsTestCtrl', function ($scope, $q, notificationsService) {

    $scope.users = [];

    $scope.add_user = function () {
        $scope.users.push({id: $scope.user_id});
        $scope.user_id = '';
    };

    $scope.remove_user = function (user) {
        $scope.users = _.without($scope.users, user);
    };

    $scope.send_test = function () {
        $scope.is_sending = true;
        var promises = [];
        var payload = {
          message: $scope.message
        };

        _.each($scope.users, function (user) {
            promises.push(notificationsService.sendTestNotification({
                payload: angular.toJson(payload),
                user_id: user.id
            }));
        });

        $q.all(promises).then(function (data) {
            toastr.success('Test notifications sent');

            // Reset form
            $scope.message = '';
            $scope.users = [];
        }).finally(function () {
            $scope.is_sending = false;
        });

    };

});

/**
 * Notifications Push Controller
 */
app.controller('NotificationsPushCtrl', function ($scope, $q, segmentsService, notificationsService) {

    // Init
    $scope.enable_link = '0';
    $scope.segment_id = '999999';
    $scope.is_loading_segments = true;

    // Get segments
    segmentsService.getSegments().then(function (data) {
        $scope.is_loading_segments = false;
        $scope.segments = data;
    });

    // Send notification
    $scope.send_notification = function () {
        if($scope.segment_id === '999999') {
            if(!confirm('Please note that the push notification you are about to send will go to everyone. Are you sure you want to continue?')) {
                return false;
            }
        }

        $scope.is_sending = true;

        var post_data = {
            message: $scope.message,
            enable_link: $scope.enable_link,
            link_to: $scope.link_to || '',
            send_to_segment_id: $scope.segment_id
        };

        notificationsService.sendNotification(post_data).then(function() {
            toastr.success('Push notification successfully submitted');

            // Reset form
            $scope.message = '';
            $scope.enable_link = '0';
            $scope.link_to = '';
            $scope.send_to_segment_id = '999999';
        }, function() {
            toastr.error('Omneo API error');
        }).finally(function() {
            $scope.is_sending = false;
        });

    };
});

/**
 * Notifications service
 */
app.factory('notificationsService', function ($http, $q) {
    return {

        getNotifications: function () {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    action: 'notifications_send_request',
                    data: {
                        api_request: 'pushnotificationlog',
                        verb: 'get'
                    }
                }
            }).then(function (response) {
                if (typeof response.error !== 'undefined') {
                    return $q.reject(response.error.message);
                }

                return response.data;
            });
        },

        sendNotification: function (data) {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    action: 'notifications_send_request',
                    data: {
                        api_request: 'pushnotifysegment',
                        verb: 'post',
                        data: data
                    }
                }
            }).then(function (response) {
                if (typeof response.error !== 'undefined') {
                    return $q.reject(response.error.message);
                }

                return response.data.data;
            });
        },

        sendTestNotification: function (data) {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    action: 'notifications_send_request',
                    data: {
                        api_request: 'sendpushnotification',
                        verb: 'post',
                        data: data
                    }
                }
            }).then(function (response) {
                if (typeof response.error !== 'undefined') {
                    return $q.reject(response.error.message);
                }

                return response.data;
            });
        }
    };
});

/**
 * Segment service
 */
app.factory('segmentsService', function ($http, $q) {
    return {
        /**
         * Get segments
         */
        getSegments: function () {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    action: 'segments_send_request',
                    data: {
                        verb: 'get',
                        api_request: 'segments'
                    }
                }
            }).then(function (response) {
                if (typeof response.data.error !== 'undefined') {
                    return $q.reject(response.data.error.message);
                } else if (response.data === 'null') {
                    return $q.reject('Omneo API Error');
                }

                return response.data.data;
            });
        },

        postSegment: function (data) {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    'action': 'segments_send_request',
                    'data': {
                        'verb': 'post',
                        'api_request': 'segments',
                        'data': data
                    }
                }
            }).then(function (response) {
                if (typeof response.error !== 'undefined') {
                    return $q.reject(response.error.message);
                }

                return response.data;
            });
        }
    };
});