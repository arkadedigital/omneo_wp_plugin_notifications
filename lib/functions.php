<?php namespace Omneo\Notifications;

use Omneo\Core;

defined('ABSPATH') or die('Access Denied');

/**
 * Push notifications admin menu
 */
function menu()
{
    add_menu_page(
        'Push Notifications',
        'Notifications',
        'manage_options',
        'push-notifications',
        __NAMESPACE__ . '\\page_notifications',
        'dashicons-email-alt',
        22
    );

    add_submenu_page(
        null,
        'Push Notifications',
        'Push Notifications',
        'manage_options',
        'push-notifications-add',
        __NAMESPACE__ . '\\page_add'
    );
}
add_action('admin_menu', __NAMESPACE__ . '\\menu');


// Push notifications page
function page_notifications()
{
    // Load page
    require_once(__PATH() . '/lib/page-notifications.php');
}

/**
 * Load assets
 */
function load_assets()
{
    if(isset($_GET['page']) && in_array($_GET['page'], ['push-notifications'])) {
        wp_enqueue_script('jquery-ui', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'jquery-ui.min.js');
        wp_enqueue_script('lodash', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'lodash.min.js');
        wp_enqueue_script('moment', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'moment.min.js');
        wp_enqueue_style('chosen', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'chosen.min.css');
        wp_enqueue_script('chosen', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'chosen.jquery.min.js');
        wp_enqueue_style('toastr', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'toastr.min.css');
        wp_enqueue_script('toastr', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'toastr.min.js');
        wp_enqueue_script('angular', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'angular.min.js');
        wp_enqueue_script('angular-route', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'angular-route.min.js');
        wp_enqueue_script('notifications', plugins_url() . DS . 'omneo-notifications' . DS . 'assets' . DS . 'main.js');
    }
}
load_assets();

function notifications_send_request()
{
    $data = json_decode(stripslashes($_GET['data']), true);
    $data['data']['content_item_id'] = $data['data']['link_to'];

    $response = Core\send_request($data);
    $json = json_encode($response);
    echo $json;
    exit;
}


/*
function notifications_send_request()
{
    global $CONTENT_TYPE_LIST;

    $data = json_decode(stripslashes($_GET['data']), true);

    $link_to = explode(',', $data['data']['link_to']);

    if($link_to[0] == 'content_items')
    {
        $type = $CONTENT_TYPE_LIST[$link_to[1]];
    }else
    {
         $type = 'views';
    }

    $args =  [
        'api_request' => 'content/'. $type .'/' . $link_to[2],
        'verb' => 'get'
    ];

    $response = Core\send_request($args);





    if($response['error'])
    {
        $json = json_encode($response);

    }else
    {
       //get appconfig

        $appconfig = json_decode(file_get_contents(get_field('omneo_api_url', 'option') . '/api/' .  get_field('omneo_api_version', 'option') .'/appconfig' ), true);

        $schemeUrl =  $appconfig['data']['schemeUrl'];

        $content_item_id = $response['data']['content_item_id'];

        $data['data']['link_to'] =  $schemeUrl . ( $type=='views' ?'view' : 'content') .'/'. $content_item_id;

        $response = Core\send_request($data);

        $json = json_encode($response);
    }
    echo $json;
    exit;
}*/

add_action('wp_ajax_notifications_send_request', __NAMESPACE__ . '\\notifications_send_request');
