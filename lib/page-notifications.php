<div class="wrap" ng-app="notifications">
    <h1>Push Notifications</h1>

    <p>A Push Notification is a message app users receive containing some text and a link.</p>


    <table style="width:100%;">
        <tr>
            <td valign="top" style="width:60%;">
                <div class="postbox" ng-controller="NotificationsPushCtrl">
                    <div class="inside">
                        <h3>Send Push Notification</h3>

                        <form name="notification_form">
                            <div class="acf-label">
                                <strong>Message</strong>

                                <p class="description">Type the message that will be included in the notification.
                                    Character Limit: 140 characters.</p>
                            </div>

                            <textarea rows="5" style="width:100%;" name="message" ng-model="message" ng-maxlength="140"
                                      ng-disabled="is_sending" placeholder="Enter message" required></textarea>

                            <div ng-if="!notification_form.message.$error.maxlength">{{ message.length || 0 }}
                                Characters
                            </div>
                            <div
                                ng-if="notification_form.message.$error.maxlength">140 character limit exceeded
                            </div>

                            <hr>

                            <table style="width:100%;">
                                <tr>
                                    <td style="width:150px;"><strong>Enable Link</strong></td>
                                    <td>
                                        <label>
                                            <input type="radio" name="enable_link" ng-model="enable_link" value="0"
                                                   ng-disabled="is_sending" required>
                                            No</label> &nbsp;&nbsp;
                                        <label><input type="radio" name="enable_link" ng-disabled="is_sending"
                                                      ng-model="enable_link"
                                                      value="1"
                                                      required>
                                            Yes</label> &nbsp;&nbsp;
                                    </td>
                                </tr>
                                <tr ng-show="enable_link=='1'">
                                    <td><strong>Link To</strong></td>
                                    <td>
                                        <?php
                                        $query = new \WP_Query(['post_type' => array('content_items', 'views'), 'posts_per_page' => -1,  'post_status' => 'publish']);
                                        ?>

                                        <select ng-model="link_to" ng-disabled="is_sending || enable_link=='0'" ng-required="enable_link=='1'">
                                            <option value=""></option>
                                            <?php if ($query->have_posts()): ?>
                                                <?php while ($query->have_posts()):

                                                    $query->the_post();
                                                    $omneo_content_item_id = get_field('omneo_content_item_id',get_the_ID());
                                                    ?>
                                                    <option value="<?php echo $omneo_content_item_id; ?>">#<?php echo $omneo_content_item_id; ?>
                                                        - <?php echo(the_title()); ?></option>
                                                <?php endwhile; ?>
                                            <?php endif;
                                            wp_reset_postdata(); ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr><td colspan="100%"><hr></td></tr>
                                <tr>
                                    <td valign="top"><p><strong>Segment</strong></p></td>
                                    <td>
                                        <span ng-if="is_loading_segments">Loading segments...</span>
                                        <select ng-model="segment_id" ng-disabled="is_sending" ng-hide="is_loading_segments">
                                            <option value="999999">-- Everyone --</option>
                                            <option ng-repeat="segment in segments" ng-disabled="segment.done===0"
                                                    value="{{ segment.id }}">
                                                {{ segment.title }} {{ segment.done===0 ? '(Processing)' : ''}}
                                            </option>
                                        </select>
                                    </td>
                                </tr>
                                <tr ng-repeat="s in selected_segments track by $index">
                                    <td></td>
                                    <td>#{{ s.id }} {{ s.title }}</td>
                                    <td>
                                        <span class="dashicons dashicons-dismiss" ng-click="remove_selected_segment(s)"
                                              ng-if="!s.is_sending"></span>
                                    </td>
                                </tr>
                            </table>

                            <hr>
                            <button type="button" class="button button-primary"
                                    ng-disabled="!notification_form.$valid"
                                    ng-click="send_notification()">
                                Push
                            </button>

                            <span class="spinner" style="float:none;" ng-class="{'is-active': is_sending}"></span>
                        </form>
                    </div>
                </div>
            </td>
            <td valign="top">
                <div class="postbox" ng-controller="NotificationsTestCtrl">
                    <div class="inside">
                        <h3>Test Notification</h3>

                        <p class="help">Test users should have the App installed in order to receive the
                            notification.</p>

                        <form name="test_notification_form">

                            <textarea id="test-message" style="width:100%;" rows="5" placeholder="Enter message" ng-model="message"
                                      required></textarea>

                            <input type="text" id="test-user-id" ng-model="user_id" placeholder="Enter user id">
                            <button type="button" id="add-user-button" class="button" ng-click="add_user();" ng-disabled="is_sending || !user_id">+
                            </button>
                        </form>

                        <div ng-if="users.length > 0">
                            <hr>
                            <table>
                                <tr ng-repeat="user in users track by $index">
                                    <td>User ID: <strong>{{ user.id }}</strong></td>
                                    <td>
                                        <div class="dashicons dashicons-dismiss" ng-click="remove_user(user)"></div>

                                    </td>
                                </tr>
                            </table>

                            <hr>

                            <button type="button" id="send-test-button" class="button button-primary" ng-click="send_test()"
                                    ng-disabled="is_sending || !test_notification_form.$valid">
                                Send test
                            </button>
                            <span class="spinner" style="float:none;" ng-class="{'is-active': is_sending}"></span>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <div ng-controller="NotificationsListCtrl">

        <table class="wp-list-table widefat striped posts" ng-show="notifications.length > 0">
            <thead>
            <tr>
                <td>ID</td>
                <td>User ID</td>
                <td>Device ID</td>
                <td>Message</td>
                <td>Created</td>
            </tr>
            </thead>

            <tbody>
            <tr ng-repeat="notification in notifications">
                <td>{{ notification.id }}</td>
                <td>{{ notification.user_id }}</td>
                <td>{{ notification.device_id }}</td>
                <td>{{ notification.message }}</td>
                <td>{{ notification.created_at }}</td>
            </tr>
            </tbody>
        </table>

        <div ng-if="notifications.length === 0"><p>No notifications</p></div>
    </div>
</div>

